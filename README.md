# Open source website monitoring

## Project goals
1. Be able to monitor anything
  * I want to be able to pull data from endpoints (http request, is your website up, api calls)
  * I want to be able to accept pushed data points (heartbeat, webhook integration)
1. Send out notifications on important events
  * Notifications should only be sent when the user needs to do something - exceptions/things broken
  * The user can configure any notifications he or she wishes to receive - perhaps there is an invoice that goes out every day
1. Display on a dashboard things that are working, broken, etc.
  * Users can expand upon the data to see more data points.