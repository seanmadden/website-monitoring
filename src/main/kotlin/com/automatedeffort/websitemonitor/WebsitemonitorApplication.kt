package com.automatedeffort.websitemonitor

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
open class WebsitemonitorApplication

fun main(args: Array<String>) {
    SpringApplication.run(WebsitemonitorApplication::class.java, *args)
}
