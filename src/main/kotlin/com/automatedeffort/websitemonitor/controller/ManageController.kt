package com.automatedeffort.websitemonitor

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.ModelAndView

@Controller
class ManageController {

    @RequestMapping("/manage/unsubscribe/{id}")
    fun unsubscribe(@PathVariable id: String): ModelAndView {
        return ModelAndView("manage")
    }

    @RequestMapping("/manage/{id}")
    fun manage(@PathVariable id: String): ModelAndView {
        val mav = ModelAndView("manage")
        mav.addObject("id", id)

        return mav
    }

}
