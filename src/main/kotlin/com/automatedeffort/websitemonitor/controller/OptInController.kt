package com.automatedeffort.websitemonitor

import org.hashids.Hashids
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import java.sql.DriverManager
import java.util.*

@Controller
class OptInController() {
    final val random = Random()
    final val encoder = Hashids("automatedeffort-monitoring-hash")

    @RequestMapping("/")
    fun index(): ModelAndView {
        val mav = ModelAndView("index")

        return mav
    }

    @RequestMapping("/monitor")
    fun monitor(@RequestParam(required = false) email: String?,
                @RequestParam(required = false) url: String?): ModelAndView {

        //TODO: validate the input

        val db = DriverManager.getConnection("jdbc:sqlite:monitoring.db")
        val insert = "INSERT INTO monitoring_sites(id, url, email, frequency) VALUES(?,?,?,?)"

        val statement = db.prepareStatement(insert)
        statement.setString(1, this.getShortId())
        statement.setString(2, url)
        statement.setString(3, email)
        statement.setInt(4, 60)

        statement.executeUpdate()

        return ModelAndView("monitor")

    }


    fun getShortId(): String {
        val id = random.nextInt(Int.MAX_VALUE - 1) + 1
        val shortId = encoder.encode(id.toLong())

        return shortId
    }
}