package com.automatedeffort.websitemonitor

import com.github.kittinunf.fuel.httpHead
import com.sun.javafx.collections.ImmutableObservableList
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Controller
import java.sql.DriverManager
import java.util.*

@Controller
class SiteMonitor {
    final val urls = ArrayList<Pair<String, String>>()

    init {
        val db = DriverManager.getConnection("jdbc:sqlite:monitoring.db")
        val select = "SELECT * from monitoring_sites WHERE frequency = 60"

        val statement = db.prepareStatement(select)

        val result = statement.executeQuery()

        while (result.next()) {
            urls.add(Pair(result.getString("id"), result.getString("url")))
        }

        println("urls initialized to $urls")
    }

    //60 second checks
    @Scheduled(fixedRate = 60000)
    fun checkSite() {
        urls.map { (id, second) ->
            val url = if (second.startsWith("http")) second else "http://${second}"

            println("[$id] Checking site $url")

           url.httpHead().responseString { request, response, result ->
               println(response.httpResponseHeaders)
           }

        }
    }
}
