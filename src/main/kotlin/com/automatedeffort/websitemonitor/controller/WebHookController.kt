package com.automatedeffort.websitemonitor

import org.springframework.stereotype.Controller

/**
 * https://updown.io/api#webhooks
 * updown.io will make a POST request every time an event occurs (up or down)
 */
@Controller
class WebHookController {

}
